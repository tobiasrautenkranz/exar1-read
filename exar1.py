# 
# Copyright (C) 2022  Tobias Rautenkranz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sqlite3
import zlib
import json
import hashlib
import textwrap
import re
import uuid

DEBUG=True

class ID:
    def __init__(self, id_string):
        if id_string == None or id_string == NULL_ID:
            self.id = None
        else:
            ensure_id_format(id_string)
            self.id = uuid.UUID(id_string)

    def is_valid(self):
        return self.id

    def __str__(self):
        return str(self.id)

    def __eq__(self, other):
        if isinstance(other, ID):
            return self.id == other.id
        return False
    def __ne__(self, other):
        return not self.__eq__(other)


    def __hash__(self):
        return hash(self.id)

class ElementID(ID):
    def __init__(self, id_string):
        super().__init__(id_string)

class ObjectID(ID):
    def __init__(self, id_string):
        super().__init__(id_string)

class InstanceID(ID):
    def __init__(self, id_string):
        super().__init__(id_string)
class ChangeSetID(ID):
    def __init__(self, id_string):
        super().__init__(id_string)
        

def to_id(raw_data):
    """returns the id as string from the byte array raw_data"""
    assert len(raw_data) == 16
    # https://docs.microsoft.com/en-us/dotnet/api/system.guid.tobytearray?view=net-6.0
    # Note that the order of bytes in the returned byte array is different from the string representation of a Guid value. The order of the beginning four-byte group and the next two two-byte groups is reversed, whereas the order of the last two-byte group and the closing six-byte group is the same
   # result = "{}-{}-{}-{}-{}".format(raw_data[3::-1].hex(),
   #         raw_data[5:3:-1].hex(),
   #         raw_data[7:5:-1].hex(),
   #         raw_data[8:10].hex(),
   #         raw_data[10:16].hex())
    result = str(uuid.UUID(bytes_le=raw_data))
    assert is_id(result)
    return result

def sha1(data):
    """returns the sha1 hash of data"""
    m = hashlib.sha1()
    m.update(data)
    return m.digest()

def is_id(id_string):
    """check if id_string has a valid ID format"""
    return re.fullmatch(r"[0-f]{8}(-[0-f]{4}){3}-[0-f]{12}", id_string)

def ensure_id_format(id_string):
    """raise an error if id_string has no valid ID format"""
    if not is_id(id_string):
        raise TypeError(f"{id_string} is not a valid ID")

type_id_str = {
        1: "EdfStructure",
        2: "EdfDirectory",
        3: "EdfProgram",
        4: "EdfString",
        5:  { "EdfMeasurementStep",
            "EdfSplitStep",
            "EdfJoinStep",
            "EdfInteractionStep",
            "EdfWorkflowStep",
            "EdfDecisionStep",
            "EdfPauseStep"},
        6: "EdfProtocol",
        7: "EdfAddInConfig" }

def str_to_type_id(type_str):
    """Returns the type id of the type string"""
    if type_str.endswith("Content"):
        type_str = type_str.removesuffix("Content")

    for tid, s in type_id_str.items():
        if isinstance(s, set):
            if type_str in s:
                return tid
        else:
            if type_str == s:
                return tid
    raise NameError(f"\"{type_str}\" is no type string")

LANGUGE = "en" # de en es fr ja zh


class EDFContent:
    """General EDF Content Node"""
    def __init__(self, data, type_id, parent):
        header,content = data.split("\r\n", maxsplit=1)
        self.type_id = type_id
        self.parent = parent
        if header and header.startswith("EDF V1:"):
            if header.startswith("EDF V1: ContentType=syngo.MR.ExamDataFoundation.Data."):
                self.type = header[53:-1]
                assert self.type_id == str_to_type_id(self.type)
                self.data = json.loads(content)
        else:
            raise ValueError("no EDF format")

    @property
    def value(self):
        """Returns the content of the Node"""
        if self.type == "EdfStringContent":
            texts = self.data["Texts"]
            if LANGUGE in texts:
                return texts[LANGUGE]

            # fallback; get any language
            return texts[list(texts.keys() - {"$id"})[0]]

        return json.dumps(self.data, sort_keys=True, indent=2)

    def __str__(self):
        if self.value:
            if self.type == "EdfProtocolContent":
                return f"EDFProtocolContent: {textwrap.shorten(self.value, 100)}"
            if self.type == "EdfStringContent":
                return self.value

            return f"EDFContent:\n{self.value}"

        return f"EDFContent: {self.type}\n{self.data}"

class EDFStructureContent(EDFContent):
    """Describes the protocol tree."""
    def __init__(self, data, type_id, parent):
        super().__init__(data, type_id, parent)
        assert type_id == str_to_type_id("EdfStructureContent")
        assert self.get_parent_directory_id(self.get_root_directory_id()) == None

    def get_root_directory_id(self):
        return ObjectID(self.data["RootDirectoryId"])

    def get_directory(self, root_id):
        return self.parent.get_node(root_id)

    def get_sub_directory_ids(self, rootID):
        assert isinstance(rootID, ObjectID)
        return map(ObjectID, self.data["SubdirectoryIds"][str(rootID)]["$values"])

    def get_parent_directory_id(self, directoryId):
        assert isinstance(directoryId, ObjectID)
        parentId = self.data["ParentDirectoryId"][str(directoryId)]
        if parentId == NULL_ID:
            return None
        return ObjectID(parentId)

    def get_program_elements(self):
        return map(lambda id: self.parent.get_node(ElementID(id)),
                self.data["ProgramElementIds"]["$values"])

    def get_subprograms(self, object_id):
        assert isinstance(object_id, ObjectID)
        ids = self.data["SubprogramElementIds"][str(object_id)]["$values"]
        return map(lambda id: self.parent.get_node(ElementID(id)), ids)

    def __str__(self):
        return "EDFStructureContent: Directories:" + \
            " ".join(map(str, self.get_sub_directory_ids(self.get_root_directory_id())))

class EDFDirectory(EDFContent):
    """A Directory of the EDFStructureContent"""
    def __init__(self, data, type_id, parent):
        super().__init__(data, type_id, parent)
        assert type_id == str_to_type_id("EdfDirectory")

class EDFProgram(EDFContent):
    """A program containg steps."""
    def __init__(self, data, type_id, parent):
        super().__init__(data, type_id, parent)
        assert type_id == str_to_type_id("EdfProgram")

    @property
    def first_step_id(self):
        return ObjectID(self.data["FirstStepId"])

    @property
    def first_step(self):
        return self.parent.get_node(self.first_step_id)

    @property
    def last_step_id(self):
        return ObjectID(self.data["LastStepId"])

    def get_next_step_ids(self, step_id):
        """Returns the object IDs of the next step."""
        assert isinstance(step_id, ObjectID)
        return map(lambda o: ObjectID(o["TargetId"]),
                self.data["LinksFrom"][str(step_id)]["$values"])

 #   def __str__(self):
 #       return "EdfProgram"

class EDFMeasurementStep(EDFContent):
    def __init__(self, data, type_id, parent):
        super().__init__(data, type_id, parent)
        assert type_id == str_to_type_id("EdfMeasurementStep")

    @property
    def selection_ids(self):
        return filter(lambda x: x.is_valid(), map(lambda o: ObjectID(o["SelectionId"]),
            self.parent.parent.content.data["LinksFrom"][str(self.parent.object_id)]["$values"]))
    @property
    def condition_id(self):
        return ObjectID(self.parent.parent.content.data["LinksFrom"]
                [str(self.parent.object_id)]["$values"][0]["ConditionId"])

    @property
    def condition_label(self):
        program = self.parent.parent
        label_id = ObjectID(program.content.data["Conditions"]
                [str(self.condition_id)]["Label"]["ObjectId"])
        return program.get_node(label_id).content.value

    @property
    def selection_labels(self):
        return map(lambda x:
                self.parent.parent.get_node(x).content.value, self.selection_ids)
    
    @property
    def next_steps(self):
        program = self.parent.parent
        step_ids = list(program.content.get_next_step_ids(self.parent.object_id))
        return map(program.get_node, step_ids)

class ProtocolData:
    """The protocol data of a EDFProtocolSequence."""
    def __init__(self, data):
        self.xprotocol = ""
        self.ascconvs = []
        self.ascconvs_meta = []
        ascconv_iter = re.finditer(
                r"^### ASCCONV BEGIN (.*)###$(.*)^### ASCCONV END ###.*$",
                data,
                re.MULTILINE | re.DOTALL)
        last_pos = 0
        for m in ascconv_iter:
            self.xprotocol += data[last_pos:m.start()]
            self.ascconvs_meta.append(m.group(1))
            self.ascconvs.append(m.group(2))
            last_pos = m.end()

    def __str__(self):
        return "XProtocol:\n{}\n\nascconv:\n{}\n".format(self.xprotocol,
                self.ascconvs[0])



class EDFProtocolContent(EDFContent):
    """A protocol sequence."""
    ### FIXME part of EDFMeasurementStep??
    def __init__(self, data, type_id, parent):
        super().__init__(data, type_id, parent)
        assert type_id == str_to_type_id("EdfProtocolContent")

    @property
    def protocol_data(self):
        return ProtocolData(self.data["Data"])

    def __str__(self):
        #return json.dumps(self.data["Preview"], sort_keys=True, indent=2)
        data = self.data["Preview"]
        ret = ""
        for parameter_name in data:
            if parameter_name != "$id":
                parameter = data[parameter_name]
                ret += parameter_name+"\n\t"
                if parameter["Label"] and parameter["Label"].strip():
                    ret += "{}: ".format(parameter["Label"])
                ret += "{} {}\n".format(parameter["Value"], parameter["Unit"])
        ret += "UpgradeInfo: {}".format(self.data["UpgradeInfo"])
        #print(str(ProtocolData(self.data["Data"])))
        return ret


EDF_CONTENT_CLASSES = {
    str_to_type_id("EdfStructureContent"): EDFStructureContent,
    str_to_type_id("EdfProtocolContent"): EDFProtocolContent,
    str_to_type_id("EdfMeasurementStep"): EDFMeasurementStep,
    str_to_type_id("EdfProgram"): EDFProgram
}

def make_EDFContent(data, type_id, parent):
    """Returns an EDFContent class instance of the appropriate type."""
    if type_id in EDF_CONTENT_CLASSES:
        return EDF_CONTENT_CLASSES[type_id](data,type_id, parent)
    return EDFContent(data, type_id, parent)


#ChangeSetState
change_set_state = {
## FIXME guesses
    "current": 0,
    "change": 1,
    "delete": 2
}

class ErrorNode:
    """A EDFNode that failed to parse (Allows for recovery of parsing)."""
    def __init__(self, instance_id, error):
        self.id = instance_id
        self.error = error

        self.content = self
        self.type = "Error"
        self.label = str(self.error)

    def __str__(self):
        return f"ErrorNode {self.id}: {self.error}"

def parse_tags(tags_string):
    """Returns the tags from a EDFNode as dict."""
    tags = {}
    if len(tags_string) > 0:
        assert tags_string[0] == "#"
        for tag in tags_string[1:].split("#"):
            key, value = tag.split("|", maxsplit=1)
            assert value
            assert not key in tags
            tags[key] = value
    
    return tags


class EDFNode:
    "A Node with some content and possibly some children, belonging to some branch."""
    def __init__(self, branch, instance_id):
        assert isinstance(instance_id, InstanceID)
        element = branch.db.execute(\
                ("SELECT Element_id, InstanceType, Children, Tags, ContentHash, "
                    "LabelElement_id, DescriptionElement_id, ParentElementId, ObjectID FROM Instance "
                    "WHERE Id = ?"), [str(instance_id)]).fetchall()
        assert len(element) <= 1

        if not len(element) == 1:
            raise KeyError(f"Element with Id {instance_id} not found")

        element = element[0]
        self.branch = branch


        element_id, self.type, self.children_ids, tags, self.content_hash, \
        label_element_id, description_element_id, parent_element_id, object_id \
        = element

        self.parentid = ElementID(parent_element_id)
        self.id = instance_id
        self.object_id = ObjectID(object_id)
        self.tags = parse_tags(tags)
        self.element_id = ElementID(element_id)

        if label_element_id:
            self.label_element_id = ElementID(label_element_id)
        if description_element_id:
            self.description_element_id = ElementID(description_element_id)

        assert str_to_type_id(self.type) == self.content_type

    @property
    def content_type(self):
        """Returns the type ID of the content."""
        return self.branch.db.execute("SELECT Type FROM Element WHERE Id = ?",
                [str(self.element_id)]).fetchone()[0]

    @property
    def content(self):
        """Returns the (EDF)Content of the node."""
        content = make_EDFContent(get_content(self.branch.db, self.content_hash), self.content_type, self)
        assert self.type + "Content" == content.type
        return content

    @property
    def children(self):
        """Returns the child nodes."""
        c = []
        if self.children_ids:
            for i in range(0, len(self.children_ids), 16):
                child_element_id = ElementID(to_id(self.children_ids[i:i+16]))
                c.append(self.branch.get_node(child_element_id))
        return c

    @property
    def label(self):
        """Returns the label."""
        if not hasattr(self, "label_element_id"):
            return None
        return str(self.get_node(self.label_element_id).content)

    @property
    def descrption(self):
        """Returns the description (if any) of the Node."""
        if not hasattr(self, "description_element_id"):
            return None
        return str(self.branch.get_node(self.description_element_id)[0].content)

    @property
    def parent(self):
        """Returns the parent EDFNode."""
        if not self.parentid:
            return None
        return self.branch.get_node(self.parentid)


    def get_node(self, typed_id):
        """Returns one EDFNode for the given ID, preferring the child nodes."""
        if isinstance(typed_id, ObjectID):
            if self.children:
                for c in self.children:
                    if c.object_id == typed_id:
                        return self.get_node(c.id)
        return self.branch.get_node(typed_id)

    def __str__(self):
        return "Node: {} {} parent: {}\n{}\nChildren:\n{}\n".format(self.id, self.type,
                self.parentid, self.content, "\n".join(map(lambda c: str(c.id) + "\t" + str(c.label), self.children)))



def get_content(db, content_hash):
    """returns the raw content stored for content_hash"""
    data, d_format = db.execute("SELECT Data,Format FROM Content WHERE Hash = ?",
            [content_hash]).fetchone()
    assert d_format == "DS"
    content = zlib.decompress(data, wbits=-15)
    assert sha1(content).hex() == content_hash
    return content.decode() # from utf8?? coding

NULL_ID = "00000000-0000-0000-0000-000000000000"

class Branch:
    """A Branch of the exar1 data."""
    def __init__(self, exar1, head_id):
        assert isinstance(head_id, ChangeSetID)
        self.id = head_id

        self.exar1 = exar1
        self.db = self.exar1.db
        self.set_changeset_id(self.id)

    def set_changeset_id(self, cid):
        "Sets the changeset ID and updates the element maps accordingly."""
        assert isinstance(cid, ChangeSetID)
        self.changeset_id = cid
        self.element_to_instance_map = self.get_element_to_instance_map()
        self.element_to_instance_map_delta = self.get_element_to_instance_map(self.delta_element_map_id)
        self.element_to_instance_map |= self.element_to_instance_map_delta ## overwrite with new keys

    @property
    def baseline(self):
        return self.db.execute("SELECT Baseline FROM Branch WHERE Head =?",
                [str(self.id)]).fetchone()[0]

    @property
    def name(self):
        return self.db.execute("SELECT Name FROM Branch WHERE Head =?",
                [str(self.id)]).fetchone()[0]

    @property
    def base_element_map_id(self):
        map_id = self.db.execute(("SELECT BaseElementMapId FROM ChangeSet "
                    "WHERE Id = ?"),  [str(self.changeset_id)]).fetchone()[0]
        if map_id == NULL_ID:
            return None
        return InstanceID(map_id)

    @property
    def delta_element_map_id(self):
        map_id = self.db.execute(("SELECT DeltaElementMapId FROM ChangeSet "
                    "WHERE Id = ?"),  [str(self.changeset_id)]).fetchone()[0]
        if map_id == NULL_ID:
            return None
        return InstanceID(map_id)

    def previous_changeset_ids(self, current_id):
        assert isinstance(current_id, ChangeSetID)
        ids = map(ChangeSetID, filter(None,
            self.db.execute("SELECT Parent1ChangeSet_id, Parent2Changeset_id "
            "FROM ChangeSet WHERE ID =?",
                [str(current_id)]).fetchone()))
        return list(ids)

    def element_id_to_instance_id(self, element_id, changeset_id=None):
        """Returns the instance ID for a element ID using the Changesets."""
        assert isinstance(element_id, ElementID)
        if changeset_id is None:
            changeset_id = self.changeset_id

        while True:
            result = self.exar1.db.execute(("SELECT InstanceId, state from "
                "InstanceChangeSet WHERE ElementId = ? AND ChangeSetId = ?"),
                [str(element_id), str(changeset_id)]).fetchone()

            if result:
                instance_id, state = result
                assert state == change_set_state["delete"] or instance_id != NULL_ID
                if instance_id == NULL_ID:
                    instance_id = None
                else:
                    instance_id = InstanceID(instance_id)
                return instance_id

            changeset_ids = self.previous_changeset_ids(changeset_id)
            if len(changeset_ids) > 1:
                for cid in changeset_ids[1:]:
                    try:
                        assert(cid != changeset_id)
                        instance_id = self.element_id_to_instance_id(element_id, cid)
                        if instance_id:
                            return instance_id
                    except KeyError:
                        continue
            if not changeset_ids:
                break
            changeset_id = changeset_ids[0]

        raise KeyError(f"Element with Id {element_id} not found")

    def check(self):
        assert not self.root or isinstance(self.root, EDFNode)
        for node_id in self.nodes_ids:
            assert isinstance(self.get_node(node_id), EDFNode)

    def validate_element_to_instance_map(self):
        """Returns element IDs that result in different instance IDs using the
        two methods."""
        difference = []
        for element_id, instance_id in self.element_to_instance_map.items():
            assert self.get_node(instance_id).element_id == element_id
            if not instance_id == self.element_id_to_instance_id(element_id):
                difference.append(element_id)
        return difference

    def object_id_to_instance_id(self, object_id):
        """Returns the unique instance ID for the Object ID in the branch."""
        assert isinstance(object_id, ObjectID)

        ids = self.db.execute("SELECT Id from Instance WHERE ObjectId = ?",
                [str(object_id)]).fetchall()
        assert len(ids) > 0
        ids = map(lambda i: InstanceID(i[0]), ids)

        instance_ids = self.element_to_instance_map.values()
        valid_ids = set(ids) & set(instance_ids)

        if not valid_ids:
        #    raise KeyError(f"No Object with ObjectId {object_id}")
            print(f"No Object with ObjectId {object_id}")
            ids = self.db.execute("SELECT Id from Instance WHERE ObjectId = ?",
                    [str(object_id)]).fetchall()[0]
            return InstanceID(ids[0])

        if len(valid_ids) > 1:
            raise KeyError(f"more than one valid id in: ${list(map(str,valid_ids))}")

        return next(iter(valid_ids))

    @property
    def root(self):
        """Returns the root node of the branch."""
        if len(self.nodes_ids) == 0:
            return None
        if not DEBUG:
            try:
                root = EDFNode(self, str(list(self.nodes_ids)[0]))
            except Exception as e:
                return ErrorNode(list(self.nodes_ids)[0], e)
        else:
            root = EDFNode(self, list(self.nodes_ids)[0])

        assert root.type == "EdfStructure"
        assert isinstance(root.content, EDFStructureContent)
        return root

    def get_element_to_instance_map(self, map_id = None):
        """Returns a dict mapping element IDs to instance IDs for the branch."""
        if not map_id:
            map_id = self.base_element_map_id
        instance_data = self.db.execute(
                "SELECT Data FROM ElementToInstanceMap WHERE Id = ?",
                [str(map_id)]).fetchall()
        if len(instance_data) == 0:
            return {}

        assert len(instance_data) == 1 and len(instance_data[0]) == 1
        instance_data = instance_data[0][0]

        assert len(instance_data) % (2*16) == 0

        nodes_map = {}

        for i in range(0, len(instance_data), 2 * 16):
            element_id = ElementID(to_id(instance_data[i:i+16]))
            instance_id = InstanceID(to_id(instance_data[i+16:i+32]))
            if not instance_id.is_valid():
                continue
            nodes_map[element_id] = instance_id

        return nodes_map

    @property
    def nodes_ids(self):
        """Returns the instance IDs of the child nodes."""
        return self.element_to_instance_map.values()

    def by_element_id(self, element_id):
        """Returns the EDFNode for the element ID."""
        if isinstance(element_id, str):
            element_id = ElementID(element_id)
        assert isinstance(element_id, ElementID)
        return self.get_node(self.element_to_instance_map[element_id])

    def by_object_id(self, object_id):
        return self.get_node(self.object_id_to_instance_id(ObjectID(object_id)))

    def get_node(self, typed_id):
        if isinstance(typed_id, InstanceID):
            if not DEBUG:
                try:
                    return EDFNode(self, typed_id)
                except Exception as e:
                    return ErrorNode(typed_id, e)
            else:
                return EDFNode(self, typed_id)
        if isinstance(typed_id, ElementID):
            return self.by_element_id(typed_id)
        if isinstance(typed_id, ObjectID):
            return self.by_object_id(str(typed_id))

        return self.exar1.get_node(typed_id)
    def __str__(self):
        return f"Branch: {self.id} {self.name} {self.baseline}"

class Exar1:
    """Reads a Siemens MR protocol exar1 file"""
    def __init__(self, file_name):
        """Load the .exar1 file from file_name"""
        self.db = sqlite3.connect(file_name, uri=True)
        result = self.db.execute("PRAGMA query_only")
        result = self.db.execute("PRAGMA quick_check").fetchall()
        if result[0][0] != "ok":
            raise ValueError(f"Database corrupted ${result}")

    def check(self):
        result = self.db.execute("PRAGMA integrity_check").fetchall()
        if result[0][0] != "ok":
            return f"Database corrupted ${result}"
        result = self.db.execute("PRAGMA foreign_key_check").fetchall()
        if result:
            return f"Database foreign key corrupted ${result}"

        return False

    @property
    def default_branch_id(self):
        return ChangeSetID(self.db.execute("SELECT HEAD FROM Branch WHERE Baseline !=?",
                ["-"]).fetchall()[-1][0])

    def get_branch(self, head_id=None):
        if not head_id:
            head_id = self.default_branch_id
        assert isinstance(head_id, ChangeSetID)
        return Branch(self, head_id)

    @property
    def branches(self):
        """Returns all available branches"""
        branches = []
        for branch_id in self.db.execute("SELECT Head FROM Branch"):
            branches.append(self.get_branch(ChangeSetID(branch_id[0])))

        return branches

    @property
    def changes(self):
        """Returns the changelog"""
        changes = self.db.execute("SELECT DateTime, Id, Author, Comment "
            "FROM Changeset ORDER BY DateTime")
        return changes.fetchall()

    def get_node(self, typed_id):
        """Returns one EDFNode for the given ID. Raises an Exception when more
        than one Node is found."""
        if isinstance(typed_id, InstanceID):
            return EDFNode(self, typed_id)
        if isinstance(typed_id, ElementID):
            nodes = self.by_element_id(typed_id)
            if len(nodes) > 1:
                raise KeyError(f"More than one Node for ElementID {typed_id}")
            return nodes[0]
        if isinstance(typed_id, ObjectID):
            nodes = self.by_object_id(typed_id)
            if len(nodes) > 1:
                raise KeyError(f"More than one Node for ObjectID {typed_id}")
            return nodes[0]

        raise TypeError(f"Unsupported type {type(typed_id)}")

    def get_nodes(self, typed_id):
        """Returns all EDFNodes for a given id"""
        if isinstance(typed_id, InstanceID):
            return [self.get_node(typed_id)]
        if isinstance(typed_id, ElementID):
            return self.by_element_id(typed_id)
        if isinstance(typed_id, ObjectID):
            return self.by_object_id(typed_id)

        raise TypeError(f"Unsupported type {type(typed_id)}")


    def by_element_id(self, element_id):
        """Returns all EDFNodes for the ElementID"""
        assert isinstance(element_id, ElementID)
        ids = self.db.execute("SELECT Id from Instance WHERE Element_id = ?",
                [str(element_id)]).fetchall()
        if not ids:
            raise KeyError(f"No Object with ElementId {element_id}")
        return list(map(lambda id: self.get_node(InstanceID(id[0])), ids))

    def by_object_id(self, object_id):
        """Returns all EDFNodes for the ObjectID"""
        assert isinstance(object_id, ObjectID)
        ids = self.db.execute("SELECT Id from Instance WHERE ObjectId = ?",
                [str(object_id)]).fetchall()
        if not ids:
            raise KeyError(f"No Object with ObjectId {object_id}")
        return list(map(lambda id: self.get_node(InstanceID(id[0])), ids))
