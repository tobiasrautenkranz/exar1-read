.DELETE_ON_ERROR: ## used for signature check / downloading

.PHONY: fetch
fetch: samples/sample1.exar1 samples/sample2.exar1

samples/sample1.exar1:
	mkdir -p samples
	curl -o $@ https://cdn0.scrvt.com/39b415fb07de4d9656c7b516d8e2d907/1800000003776748/7d9e7cd34959/Skyra_Schulter_BLADE.e_1800000003776748xar1
	echo "0108f4ab9e40acb830e04118594e9085e2faeee1c8c29aa56b51863fe11b400d $@" | sha256sum -c

samples/sample2.exar1:
	mkdir -p samples
	curl -o $@ https://cdn0.scrvt.com/39b415fb07de4d9656c7b516d8e2d907/dbc049a2951509ac/110b366691a8/goknee2d_lumina-06777207.exar1
	echo "d176027df901135807cc0a606e5d98c7890a6da5bb5a8b82f099a675671d1094 $@" | sha256sum -c

.PHONY: test
test: fetch
	./exar1_cli.py check samples/sample1.exar1 > /dev/null
	./exar1_cli.py check samples/sample2.exar1 > /dev/null
	./exar1_cli.py branches samples/sample1.exar1 > /dev/null
	./exar1_cli.py changes samples/sample1.exar1 > /dev/null
	./exar1_cli.py print samples/sample1.exar1 > /dev/null
