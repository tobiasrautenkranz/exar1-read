# Read Siemens MRI .exar1 Protocol Files

Protocols of Siemens MRI scanners can be shared using files with the _.exar1_
extension. The code in this repository is the result of and allows for the
exploration of these files.

## File Format

The exar1 files are a sqlite database. (If you find a file with the
_.exar1-journal_ extension something went wrong
[1](https://web.archive.org/web/20220204135411/https://www.siemens-healthineers.com/en-us/magnetic-resonance-imaging/magnetom-world/clinical-corner/protocols/body-protocols))

The database contains several tables describing the branches, changesets,
elements, instances and contents and their relations. These objects are
identified by an UUID or a sha1 hash for the contents. An node is uniquely
identified by its instance ID. It my have some child and a parent node. It also
has an associated content that is zlib compressed without header. A node also
has an element and object ID; but these are more general and thus several
instances may share them. This probably can be used to track changes. To find
the right instance ID for a given element or object ID it is best to search
within the child nodes.

The decompressed content stats with the header
`EDF V1: ContentType=syngo.MR.ExamDataFoundation.Data.`
followed by the type of data (e.g. `EdfStringContent`) and a newline `\r\n`.
The rest is in JSON.

## Example

```
./exar1_cli.py print samples/sample1.exar1
Root                                    EdfDirectory        8a27f421-beec-4c9d-acc0-63461da9fe5f
 New Tree                               EdfDirectory        87a1fde0-881b-4db4-ba0d-5323ae6a7136
  Schulter                              EdfDirectory        dfc7854e-f2d1-4a26-aa47-504a1b098ebd
   Untersuchung                         EdfDirectory        87121087-edd4-4ac1-af2a-b1ae41588315
    BLADE                               EdfProgram          3ab257e2-2429-4ab2-8ed1-92f95a929fee
     Patient View                       EdfWorkflowStep     00fe362a-e2d3-4f18-8b21-f9686fae9ce4
     <SplitStep>                        EdfSplitStep        6253f369-fbd0-4e05-9c77-cadec349e41c
     localizer                          EdfMeasurementStep  f7df1f28-4404-4e5d-91f1-79d0cf812a4b
     localizer_tra_cor                  EdfMeasurementStep  6aa7eba3-ec03-4883-9758-d5e9ab81f280
     localizer_sag                      EdfMeasurementStep  54ce996c-84a0-44d6-a013-1c118e68b453
     t1_BLADE_cor                       EdfMeasurementStep  dd4cc339-f265-4fba-ae4e-4bdf6e4f4ec8
     pd_BLADE_fs_cor                    EdfMeasurementStep  131d16d7-2f17-4de9-98d3-5e98c45b1b28
     t2_BLADE_sag                       EdfMeasurementStep  351d6b46-108c-4c4c-8f4c-1a2da7db0abc
     pd_BLADE_fs_tra                    EdfMeasurementStep  14c81da7-a875-4194-9efe-e1153e4a2446
     Kontrastmittel                     EdfPauseStep        9c6679a0-f93c-46a4-a67b-ab01aba41282
     <JoinStep>                         EdfJoinStep         9aeee33f-98c6-4368-bd65-646f5696b717
     t1_BLADE_fs_cor_KM                 EdfMeasurementStep  7463c18e-b00e-400c-b461-f79dc486ce12
     t1_BLADE_fs_sag_KM                 EdfMeasurementStep  42f15906-8c0f-4030-8796-196d1363469f
```
