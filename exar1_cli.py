#!/usr/bin/env python3
# 
# Copyright (C) 2022  Tobias Rautenkranz
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import sys
import exar1
import itertools

class TreePrinter:
    def __init__(self):
        self.typeLength = 20
        self.labelLength = 40
        self.verbose = True
        self.maxDepth = 500

    def indent(self, level):
        return "".rjust(self.n_indent(level), " ")
    def n_indent(self, level):
        return level

    def print_entry(self, level, label, typeString, idString):
        if level > self.maxDepth:
            return
        if self.verbose:
            print(f"{self.indent(level)+label[:self.labelLength-self.n_indent(level)-1]:<{self.labelLength}}{typeString:<{self.typeLength}}{idString}")
        else:
            print(self.indent(level)+label)


    def print_node(self, node, level):
        self.print_entry(level, node.label, node.type, node.id)

    def print(self, node, level=0):
        if not node:
            return
        if isinstance(node.content, exar1.EDFStructureContent):
            self.print_directory(node.content, level, node.content.get_root_directory_id())
            self.print_subprograms(node.content, level, node.content.get_root_directory_id())
        if isinstance(node.content, exar1.EDFProgram):
            self.print_program(node.content, level+1)
        if isinstance(node.content, exar1.ErrorNode):
            self.print_node(node.content, level+1)
        #else:
         #   for c in node.children:
         #       tree(c, level+1)

    def print_directory(self, edfStructure, level, rootID):
        rootNode = edfStructure.get_directory(rootID)
        self.print_node(rootNode, level)
        self.print_subprograms(edfStructure, level+1, rootID)
        for sId in edfStructure.get_sub_directory_ids(rootID):
            self.print_directory(edfStructure, level+1, sId)

    def print_subprograms(self, edfStructure, level, rootID):
        for sp in edfStructure.get_subprograms(rootID):
            self.print_node(sp, level)
            if isinstance(sp.content, exar1.EDFProgram):
                self.print_program(sp.content, level+1)
            #tree(sp, level+1)

    def print_program(self, edfProgram, level):
        self.print_steps(edfProgram.first_step, level)

    def print_steps(self, step, level):
        self.print_node(step, level)
        next_steps = list(step.content.next_steps)
        l = level
        if len(next_steps) > 1:
            l = level+1
        for s, label in itertools.zip_longest(next_steps, step.content.selection_labels):
            if l > level:
                self.print_entry(level,
                        label, "Condition",
                        step.content.condition_label)
            self.print_steps(s, l)


def find_id(e, any_id):
    ids = [exar1.InstanceID(any_id), exar1.ElementID(any_id), exar1.ObjectID(any_id)]
    results = {}
    for i in ids:
        try:
            oo = e.get_nodes(i)
            results[type(i).__name__] = oo
        except KeyError:
            pass

    return results

def main():
    parser = argparse.ArgumentParser(description=
            "Access a Siemens MRI exar1 protocol file")
    subparsers = parser.add_subparsers(dest="command")
    parser.add_argument("filename", metavar="FILENAME", help="exar1 to read")

    print_parser = subparsers.add_parser("print", help="prints content")
    print_parser.add_argument("--id", type=exar1.ObjectID,
            help="Fetch Element by Instance ID")
    print_parser.add_argument("--elementId", type=exar1.ElementID,
            help="Fetch Element by ElementID")
    print_parser.add_argument("--objectId", type=exar1.ObjectID,
            help="Fetch Element by objectId")
    print_parser.add_argument("--anyId", help="Fetches Element by an ID of any type")
    print_parser.add_argument("--branch", help="Fetch branch by ID")
    print_parser.add_argument("--changesetId", type=exar1.ChangeSetID,
            help="Set changeset ID")
    print_parser.add_argument("--maxDepth", type=int,
        help="Set the maximal depth of the program tree to print")

    check_parser = subparsers.add_parser("check", help="Check DB integrity")
    branch_parser = subparsers.add_parser("branches",
        help="Prints the available branches")
    changelog_parser = subparsers.add_parser("changes", help="Prints changelog")

    args = parser.parse_args()

    e = exar1.Exar1("file:" + args.filename + "?mode=ro")

    if args.command == "check":
        print("checking file...", end="", flush=True)
        r = e.check()
        if r:
            print("failed")
            print(r)
            sys.exit(1)
        print("OK")

        print("checking branches...", end="", flush=True)
        for branch in e.branches:
            branch.check()
        print("OK")

        n_branches = len(e.branches)
        ## FIXME check all changesets
        for i, branch in enumerate(e.branches):
            print(f"\rchecking element_to_instance_map ({i+1}/{n_branches})...",
                    end="", flush=True)
            r = branch.validate_element_to_instance_map()
            if r:
                print("failed")
                print(r)
                sys.exit(1)
        print("OK")

        sys.exit(0)

    elif args.command == "branches":
        for b in e.branches:
            print(f"{b.id}\t{b.name}\t{b.baseline}")
    elif args.command == "changes":
        for c in e.changes:
            print("\t".join(c))
    else:
        if args.id:
            print(e.get_node(args.id))
        elif args.elementId:
            print(e.get_node(args.elementId))
        elif args.objectId:
            print(e.get_node(args.objectId))
            exar1.ensure_id_format(args.objectId)
        elif args.anyId:
            exar1.ensure_id_format(args.anyId)
            objects = find_id(e, args.anyId)
            for t_id, obj in objects.items():
                print("{} {} found\n{}\n".format(t_id, args.anyId, obj))
        else:
            tree = TreePrinter()
            branch = e.get_branch()
            if args.changesetId:
                branch.set_changeset_id(args.changesetId)
            if args.maxDepth:
                tree.maxDepth = args.maxDepth
            tree.print(branch.root)

if __name__ == "__main__":
    main()
